#-------------------------------------------------
#
# Project created by QtCreator 2017-11-07T22:52:30
#
#-------------------------------------------------

#CONFIG += NoPoloWSS # unremark to dissable WSS support from Polo

TEMPLATE = subdirs

SUBDIRS = \
          polo \
          example

CONFIG += ordered

polo.subdir     = src/polo
example.subdir  = src/example


