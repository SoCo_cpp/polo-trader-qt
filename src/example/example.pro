#-------------------------------------------------
#
# Project created by QtCreator 2017-11-07T22:52:30
#
#-------------------------------------------------

QT       *= core gui

greaterThan(QT_MAJOR_VERSION, 4): QT *= widgets

TARGET = polotrade
TEMPLATE = app

SOURCES  += main.cpp\
            mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui


LIBS += -L../polo -lpolo
INCLUDEPATH += ../polo
