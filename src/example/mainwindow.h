#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class Polo;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void setAccountKeySecret(const QString& key, const QString& secret);

protected:
    Polo* polo_;
    QString key_;
    QString secret_;

private slots:
    void keySecretChanged();
    void on_btnPollBalances_clicked();
    void on_btnPollTickers_clicked();
    void on_btnChartData_clicked();
    void on_btnSell_clicked();
    void on_btnBuy_clicked();
    void on_btnMove_clicked();
    void on_btnOrderTrades_clicked();
    void on_btnCancelOrder_clicked();
    void on_btnTradeHistory_clicked();
    void on_btnOpenOrders_clicked();
    void on_btnOrderBook_clicked();
    void on_btnDepositAddresses_clicked();
    void on_btnGenerateNewAddress_clicked();
    void on_btnWithdraw_clicked();
    void on_btnSubscribeTicker_toggled(bool checked);
    void on_btnSubscribeOrderBook_toggled(bool checked);

    void poloFailed();
    void poloSuccess();
    void poloBalancesUpdated();
    void poloTickersUpdated();
    void poloChartDataUpdated();
    void poloBuySellMove(unsigned long orderNumber);
    void poloTradeOrdersUpdated();
    void poloOrderBookUpdated(QString currencyPair);
    void poloDepositAddressesUpdated();


private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
