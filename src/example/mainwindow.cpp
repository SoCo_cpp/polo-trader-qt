#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>

#include <QMutex>
#include <QMutexLocker>
#include <QVariantMap>

#include <polo.h>
#include <polohttp.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    polo_(new Polo()),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    Q_ASSERT(polo_);
    connect(polo_, SIGNAL(failed()), this, SLOT(poloFailed()));
    connect(polo_, SIGNAL(success()), this, SLOT(poloSuccess()));
    connect(polo_, SIGNAL(balancesUpdated()), this, SLOT(poloBalancesUpdated()));
    connect(polo_, SIGNAL(tickersUpdated()), this, SLOT(poloTickersUpdated()));
    connect(polo_, SIGNAL(chartDataUpdated()), this, SLOT(poloChartDataUpdated()));
    connect(polo_, SIGNAL(buysellmove(unsigned long)), this, SLOT(poloBuySellMove(unsigned long)));
    connect(polo_, SIGNAL(tradeOrdersUpdated()), this, SLOT(poloTradeOrdersUpdated()));
    connect(polo_, SIGNAL(orderBookUpdated(QString)), this, SLOT(poloOrderBookUpdated(QString)));
    connect(polo_, SIGNAL(depositAddressesUpdated()), this, SLOT(poloDepositAddressesUpdated()));

    polo_->setUrls(QUrl("https://poloniex.com/tradingApi"), QUrl("https://poloniex.com/public"), QUrl("wss://api2.poloniex.com"));
    polo_->autoOpenWSS();

    ui->dtChartDataStart->setDateTime(QDateTime::currentDateTime().addDays(-3));
    ui->dtChartDataEnd->setDateTime(QDateTime::currentDateTime());
    ui->dtTradeHistoryStart->setDateTime(QDateTime::currentDateTime().addSecs(-21600/*6 hours*/));
    ui->dtTradeHistoryEnd->setDateTime(QDateTime::currentDateTime());
    connect(ui->txtKey, SIGNAL(textChanged(QString)), this, SLOT(keySecretChanged()));
    connect(ui->txtSecret, SIGNAL(textChanged(QString)), this, SLOT(keySecretChanged()));
}

MainWindow::~MainWindow()
{
    delete ui;
    if (polo_)
        delete polo_;
}

void MainWindow::setAccountKeySecret(const QString& key, const QString& secret)
{
    Q_ASSERT(polo_);
    key_= key;
    secret_ = secret;
    polo_->setKeySecret(key, secret);
    disconnect(ui->txtKey, SIGNAL(textChanged(QString)), this, SLOT(keySecretChanged()));
    disconnect(ui->txtSecret, SIGNAL(textChanged(QString)), this, SLOT(keySecretChanged()));
    ui->txtKey->setReadOnly(true);
    ui->txtSecret->setReadOnly(true);
    ui->txtKey->setText("(specified by command argument)");
    ui->txtSecret->setText("(specified by command argument)");
}

void MainWindow::on_btnPollBalances_clicked()
{
    Q_ASSERT(polo_);
    qDebug() << "on_btnPollBalances_clicked";

    if (!polo_->requestBalances(ui->cbBalancesTradable->isChecked()))
    {
        qDebug() << "polo requestBalances failed: " << polo_->lastError();
    }
}

void MainWindow::on_btnPollTickers_clicked()
{
    Q_ASSERT(polo_);
    if (!polo_->requestTickers())
    {
        qDebug() << "polo requestTickers failed: " << polo_->lastError();
    }
}

void MainWindow::on_btnChartData_clicked()
{
    Q_ASSERT(polo_);
    qDebug() << "on_btnPollBalances_clicked";
    if (!polo_->requestChartData(ui->cbChartDataCurrency->currentText(), ui->dtChartDataStart->dateTime(), ui->dtChartDataEnd->dateTime(), ui->cbChartDataPeriod->currentText().toInt()))
    {
        qDebug() << "polo requestChartData failed: " << polo_->lastError();
    }
    else qDebug("http request success");
}

void MainWindow::on_btnSell_clicked()
{
    Q_ASSERT(polo_);
    qDebug() << "on_btnSell_clicked";
    bool ok;
    qreal rate = ui->txtBuySellRate->text().toDouble(&ok);
    if (!ok)
    {
        qDebug() << "sell - invalid rate specified";
        return;
    }
    qreal amount = ui->txtBuySellAmount->text().toDouble(&ok);
    if (!ok)
    {
        qDebug() << "sell - invalid amount specified";
        return;
    }
    if (!polo_->requestSell(ui->cbBuySellCurrency->currentText(), rate, amount))
    {
        qDebug() << "polo requestSell failed: " << polo_->lastError();
    }
}

void MainWindow::on_btnBuy_clicked()
{
    Q_ASSERT(polo_);
    qDebug() << "on_btnBuy_clicked";
    bool ok;
    qreal rate = ui->txtBuySellRate->text().toDouble(&ok);
    if (!ok)
    {
        qDebug() << "buy - invalid rate specified";
        return;
    }
    qreal amount = ui->txtBuySellAmount->text().toDouble(&ok);
    if (!ok)
    {
        qDebug() << "buy - invalid amount specified";
        return;
    }
    if (!polo_->requestBuy(ui->cbBuySellCurrency->currentText(), rate, amount))
    {
        qDebug() << "polo requestBuy failed: " << polo_->lastError();
    }
}

void MainWindow::on_btnMove_clicked()
{
    Q_ASSERT(polo_);
    qDebug() << "on_btnMove_clicked";
    bool ok;
    qreal rate = ui->txtBuySellRate->text().toDouble(&ok);
    if (!ok)
    {
        qDebug() << "move - invalid rate specified";
        return;
    }
    qreal amount;
    if (ui->txtBuySellAmount->text().isEmpty())
        amount = -1.0; // special value indicate's no change
    else
    {
        amount = ui->txtBuySellAmount->text().toDouble(&ok);
        if (!ok)
        {
            qDebug() << "move - invalid amount specified";
            return;
        }
    }
    unsigned long orderNumber = ui->txtBuySellOrderNumber->text().toLong(&ok);
    if (!ok)
    {
        qDebug() << "move - invalid order number specified";
        return;
    }
    if (!polo_->requestMoveOrder(orderNumber, rate, amount))
    {
        qDebug() << "polo requestMoveOrder failed: " << polo_->lastError();
    }
}

void MainWindow::on_btnOrderTrades_clicked()
{
    Q_ASSERT(polo_);
    qDebug() << "on_btnOrderTrades_clicked";
    bool ok;
    unsigned long orderNumber = ui->txtOrderNumber->text().toLong(&ok);
    if (!ok)
    {
        qDebug() << "Order Trades - invalid Order Number specified";
        return;
    }
    if (!polo_->requestOrderTrades(orderNumber))
    {
        qDebug() << "polo requestOrderTrades failed: " << polo_->lastError();
    }
}

void MainWindow::on_btnCancelOrder_clicked()
{
    Q_ASSERT(polo_);
    qDebug() << "on_btnCancelOrder_clicked";
    bool ok;
    unsigned long orderNumber = ui->txtOrderNumber->text().toLong(&ok);
    if (!ok)
    {
        qDebug() << "Cancel Order - invalid Order Number specified";
        return;
    }
    if (!polo_->requestCancelOrder(orderNumber))
    {
        qDebug() << "polo requestCancelOrder failed: " << polo_->lastError();
    }
}

void MainWindow::on_btnTradeHistory_clicked()
{
    Q_ASSERT(polo_);
    qDebug() << "on_btnTradeHistory_clicked";
    if (!polo_->requestTradeHistory(ui->cbTradeHistoryCurrency->currentText(), ui->dtTradeHistoryStart->dateTime(), ui->dtTradeHistoryEnd->dateTime()))
    {
        qDebug() << "polo requestTradeHistory failed: " << polo_->lastError();
    }
}

void MainWindow::on_btnOpenOrders_clicked()
{
    Q_ASSERT(polo_);
    qDebug() << "on_btnTradeHistory_clicked";
    if (!polo_->requestOpenOrders(ui->cbOpenOrdersCurrency->currentText()))
    {
        qDebug() << "polo requestOpenOrders failed: " << polo_->lastError();
    }
}

void MainWindow::on_btnOrderBook_clicked()
{
    Q_ASSERT(polo_);
    qDebug() << "on_btnOrderBook_clicked";
    if (!polo_->requestOrderBook(ui->cbOrderBookCurrency->currentText(), ui->txtOrderBookDepth->text().toUInt()))
    {
        qDebug() << "polo requestOrderBook failed: " << polo_->lastError();
    }
}

void MainWindow::on_btnDepositAddresses_clicked()
{
    Q_ASSERT(polo_);
    qDebug() << "on_btnDepositAddresses_clicked";
    if (!polo_->requestDepositAddresses())
    {
        qDebug() << "polo requestDepositAddresses failed: " << polo_->lastError();
    }
}

void MainWindow::on_btnGenerateNewAddress_clicked()
{
    Q_ASSERT(polo_);
    qDebug() << "on_btnGenerateNewAddress_clicked";
    if (!polo_->requestGenerateNewAddress(ui->cbGenerateNewAddressCurrency->currentText()))
    {
        qDebug() << "polo requestGenerateNewAddress failed: " << polo_->lastError();
    }
}

void MainWindow::on_btnWithdraw_clicked()
{
    Q_ASSERT(polo_);
    qDebug() << "on_btnWithdraw_clicked";
    if (!polo_->requestWithdraw(ui->cbWithdrawCurrency->currentText(), ui->txtWithdrawAmount->text().toDouble(), ui->txtWithdrawAddress->text()))
    {
        qDebug() << "polo requestWithdraw failed: " << polo_->lastError();
    }
}

void MainWindow::on_btnSubscribeTicker_toggled(bool checked)
{
    Q_ASSERT(polo_);
    qDebug() << "on_btnSubscribeTicker_toggled checked: " << checked;

    if (checked)
    {
        if (!polo_->subscribeTicker())
            qDebug() << "polo subscribeTicker failed: " << polo_->lastError();
    }
    else
    {
        if (!polo_->unsubscribeTicker())
            qDebug() << "polo unsubscribeTicker failed: " << polo_->lastError();
    }
}

void MainWindow::on_btnSubscribeOrderBook_toggled(bool checked)
{
    Q_ASSERT(polo_);
    qDebug() << "on_btnSubscribeOrderBook_toggled checked: " << checked;

    if (checked)
    {
        if (!polo_->subscribeOrderBook(ui->cbOrderBookCurrency->currentText()))
            qDebug() << "polo subscribeOrderBook failed: " << polo_->lastError();
    }
    else
    {
        if (!polo_->unsubscribeOrderBook(ui->cbOrderBookCurrency->currentText()))
            qDebug() << "polo unsubscribeOrderBook failed: " << polo_->lastError();
    }
}

void MainWindow::poloFailed()
{
    Q_ASSERT(polo_);
    qDebug() << "poloFailed: " << polo_->lastError();
    ui->txtBuySellOrderNumber->setText("Failed");
}

void MainWindow::poloSuccess()
{
    Q_ASSERT(polo_);
    qDebug() << "poloSuccess: " << polo_->lastError();
    ui->txtBuySellOrderNumber->setText("Success");
}

void MainWindow::poloBalancesUpdated()
{
    Q_ASSERT(polo_);
    qDebug() << "poloBalancesUpdated";

    QMutexLocker locker(polo_->mxBalances());
    ui->twBalances->setRowCount(polo_->balances().keys().size() * 2);
    //qDebug() << "poloBalancesUpdated rows: " << polo_->balances().keys().size();
    int r = 0;
    QStringList currencyList;
    foreach (const QString& pair, polo_->balances().keys())
    {
        foreach (const QString& currency, polo_->balances().value(pair).keys())
        {
            if (!currencyList.contains(currency))
            {
                //qDebug() << " - currency: " << currency;
                currencyList.append(currency);
                if ( ui->cbBalancesShowEmpty->isChecked() ||
                     (polo_->balances().value(pair).value(currency).canConvert(QMetaType::Float) &&
                      polo_->balances().value(pair).value(currency).toFloat() != 0.0))
                {
                    QTableWidgetItem* wiCurrency = new QTableWidgetItem(currency);
                    QTableWidgetItem* wiValue = new QTableWidgetItem(polo_->balances().value(pair).value(currency, "").toString());
                    ui->twBalances->setItem(r, 0, wiCurrency);
                    ui->twBalances->setItem(r, 1, wiValue);
                    //qDebug() << "[" << r << "] " << key << ": " << polo_->balances().value(key, "").toString();
                    r++;
                }
                //else qDebug() << * "skipped <" << pair << "," << currency << ">";
            }
        }
    }
    ui->twBalances->setRowCount(r);
}

void MainWindow::poloTickersUpdated()
{
    QString strValue;
    Q_ASSERT(polo_);
    qDebug() << "poloTickersUpdated";

    QMutexLocker locker(polo_->mxTickers());
    int r = 0; // row
    QStringList pairList;
    ui->twTickers->setRowCount(polo_->tickers().keys().size());
    ui->cbChartDataCurrency->clear();
    ui->cbBuySellCurrency->clear();
    ui->cbTradeHistoryCurrency->clear();
    ui->cbOpenOrdersCurrency->clear();
    ui->cbOpenOrdersCurrency->addItem("all");
    ui->cbOrderBookCurrency->clear();

    foreach (const QString& key, polo_->tickers().keys())
    {
        if (!pairList.contains(key))
        {
            pairList.append(key);
            ui->cbChartDataCurrency->addItem(key);
            ui->cbBuySellCurrency->addItem(key);
            ui->cbTradeHistoryCurrency->addItem(key);
            ui->cbOpenOrdersCurrency->addItem(key);
            ui->cbOrderBookCurrency->addItem(key);
        }
        QVariantList fieldValues = polo_->tickers().value(key);

        int c = 0; // column
        ui->twTickers->setItem(r, c++, new QTableWidgetItem(key));
        foreach (const QVariant& value, fieldValues)
        {
            if (value.type() == QVariant::Double)
                strValue = value.toString();
            else
                strValue = QString::number(value.toDouble(), 'f', 8/*prec*/);
            ui->twTickers->setItem(r, c++, new QTableWidgetItem(strValue));
        }

        r++;
    }
    QStringList currencyList;
    foreach (QString pair, pairList)
    {
        QStringList parts = pair.split("_");
        QString currency = (parts.size() > 1 ? parts.at(1) : pair);
        if (!currencyList.contains(currency))
            currencyList.append(currency);
    }
    ui->cbGenerateNewAddressCurrency->clear();
    ui->cbGenerateNewAddressCurrency->addItems(currencyList);
    ui->cbWithdrawCurrency->clear();
    ui->cbWithdrawCurrency->addItems(currencyList);
}

void MainWindow::poloChartDataUpdated()
{
    Q_ASSERT(polo_);
    int r = 0;
    QString strValue;
    QStringList fieldList = QStringList() << "date" << "high" << "low" << "open" << "close" << "volume" << "quoteVolume" << "weightedAverage";
    qDebug() << "chart data list size " << polo_->chartData().size();
    QMutexLocker locker(polo_->mxChartData());
    ui->twChartData->setRowCount(polo_->chartData().size());
    foreach (const QVariantMap& fieldValues, polo_->chartData())
    {
        int c = 0;
        foreach (const QString& field, fieldList)
        {
            if (field == "date")
                strValue = QDateTime::fromTime_t(fieldValues.value(field).toInt()).toString();
            else
                strValue = QString::number(fieldValues.value(field).toDouble(), 'f', 8/*prec*/);
            ui->twChartData->setItem(r, c, new QTableWidgetItem(strValue));
            //qDebug() << "[" << r << "," << c << "] " << strValue;
            c++;
        }
        r++;
    }
}

void MainWindow::poloBuySellMove(unsigned long orderNumber)
{
    qDebug() << "poloBuySellMove orderNumber: " << orderNumber;
    ui->txtBuySellOrderNumber->setText(QString::number(orderNumber));
}

void MainWindow::poloTradeOrdersUpdated()
{
    Q_ASSERT(polo_);
    QString strValue;
    qDebug() << "poloTradeOrdersUpdated";
    QMutexLocker locker(polo_->mxTradeOrders());
    ui->twTransactionsOrders->setRowCount(polo_->tradeOrders().size());
    int r = 0;
    foreach (const QVariantMap& fieldValues, polo_->tradeOrders())
    {
        // date trade id rate amount total fee
        strValue = "";
        if (fieldValues.contains("date")) // string representation of a date
            strValue = fieldValues.value("date").toString();
        ui->twTransactionsOrders->setItem(r, 0, new QTableWidgetItem(strValue));

        strValue = "";
        if (fieldValues.contains("currencyPair"))
        {
            if (fieldValues.value("currencyPair").canConvert(QMetaType::Int))
            {
                unsigned int currencyID = fieldValues.value("currencyPair").toInt();
                strValue += QString("%1 ").arg(polo_->currencyPairByID(currencyID, QString::number(currencyID)));
            }
            else
                strValue += QString("%1 ").arg(fieldValues.value("currencyPair").toString());
        }
        else if (fieldValues.contains("currencyID"))
        {
            unsigned int currencyID = fieldValues.value("currencyID").toInt();
            strValue += QString("%1 ").arg(polo_->currencyPairByID(currencyID, QString::number(currencyID)));
        }
        if (fieldValues.contains("type"))
        {
            if (fieldValues.value("type").canConvert(QMetaType::Int))
                strValue += (fieldValues.value("type").toInt() == 0 ? "sell" : "buy");
            else
                strValue += fieldValues.value("type").toString();
        }
        ui->twTransactionsOrders->setItem(r, 1, new QTableWidgetItem(strValue));

        strValue = "";
        if (fieldValues.contains("globalTradeID"))
            strValue = QString("%1 / ").arg(fieldValues.value("globalTradeID").toString());
        if (fieldValues.contains("tradeID"))
            strValue += fieldValues.value("tradeID").toString();
        if (fieldValues.contains("orderNumber"))
            strValue += fieldValues.value("orderNumber").toString();
        ui->twTransactionsOrders->setItem(r, 2, new QTableWidgetItem(strValue));

        strValue = "";
        if (fieldValues.contains("rate") && fieldValues.value("rate").canConvert(QMetaType::Double))
            strValue = QString::number(fieldValues.value("rate").toDouble(), 'f', 8/*prec*/);
        ui->twTransactionsOrders->setItem(r, 3, new QTableWidgetItem(strValue));

        strValue = "";
        if (fieldValues.contains("amount") && fieldValues.value("amount").canConvert(QMetaType::Double))
            strValue = QString::number(fieldValues.value("amount").toDouble(), 'f', 8/*prec*/);
        ui->twTransactionsOrders->setItem(r, 4, new QTableWidgetItem(strValue));

        strValue = "";
        if (fieldValues.contains("total") && fieldValues.value("total").canConvert(QMetaType::Double))
            strValue = QString::number(fieldValues.value("total").toDouble(), 'f', 8/*prec*/);
        ui->twTransactionsOrders->setItem(r, 5, new QTableWidgetItem(strValue));

        strValue = "";
        if (fieldValues.contains("fee") && fieldValues.value("fee").canConvert(QMetaType::Double))
            strValue = QString::number(fieldValues.value("fee").toDouble(), 'f', 8/*prec*/);
        ui->twTransactionsOrders->setItem(r, 6, new QTableWidgetItem(strValue));

        r++;
    }
}

void MainWindow::poloOrderBookUpdated(QString currencyPair)
{
    Q_ASSERT(polo_);
    qDebug() << "poloOrderBookUpdated: " << currencyPair;
    QMutexLocker locker(polo_->mxOrderBook());
    const Polo::OrderBook& orderBook = polo_->orderBook(currencyPair);
    ui->twAsks->setRowCount(orderBook.asks.keys().size());
    ui->twBids->setRowCount(orderBook.bids.keys().size());
    int max = 20;
    int r = 0;
    foreach (qreal rate, orderBook.asks.keys())
    {
        ui->twAsks->setItem(r, 0, new QTableWidgetItem(QString::number(rate, 'f', 8/*prec*/)));
        ui->twAsks->setItem(r, 1, new QTableWidgetItem(QString::number(orderBook.asks.value(rate), 'r', 8/*prec*/)));
        r++;
        if (r > max)
            break;
    }
    r = 0;
    foreach (qreal rate, orderBook.bids.keys())
    {
        ui->twBids->setItem(r, 0, new QTableWidgetItem(QString::number(rate, 'f', 8/*prec*/)));
        ui->twBids->setItem(r, 1, new QTableWidgetItem(QString::number(orderBook.bids.value(rate), 'r', 8/*prec*/)));
        r++;
        if (r > max)
            break;
    }
}

void MainWindow::poloDepositAddressesUpdated()
{
    Q_ASSERT(polo_);
    qDebug() << "poloDepositAddressesUpdated";
    QMutexLocker locker(polo_->mxDepositAddresses());
    ui->twDepositAddresses->setRowCount(polo_->depositAddresses().keys().size());
    int r = 0;
    foreach (const QString& currency, polo_->depositAddresses().keys())
    {
        ui->twDepositAddresses->setItem(r, 0, new QTableWidgetItem(currency));
        ui->twDepositAddresses->setItem(r, 1, new QTableWidgetItem(polo_->depositAddresses().value(currency)));
        r++;
    }
}

void MainWindow::keySecretChanged()
{
    Q_ASSERT(polo_);
    qDebug() << "keySecretChanged";
    key_= ui->txtKey->text();
    secret_ = ui->txtSecret->text();
    polo_->setKeySecret(key_, secret_);
}
