#include "mainwindow.h"
#include <QApplication>
#include <QCommandLineParser>

#define APP_NAME "PoloTrade"
#define APP_VERSION "1.0.0"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QCommandLineParser parser;

    app.setApplicationName(APP_NAME);
    app.setApplicationVersion(APP_VERSION);


    QCommandLineOption cloKey(QStringList() << "k" << "key", "Account key (Key and Secret required)", "key");
    QCommandLineOption cloSecret(QStringList() << "s" << "sec" << "secret", "Account secret (Key and Secret required)", "secret");

    parser.setApplicationDescription(APP_NAME);
    parser.addHelpOption();
    parser.addOption(cloKey);
    parser.addOption(cloSecret);
    parser.process(app);

    // Require key and secret specified by command argument
    /*
    if (!parser.isSet(cloKey) || !parser.isSet(cloSecret))
    {
        fprintf(stderr, "Account Key and Secret are required specified by command line (-s -k)\n\n");
        parser.showHelp(1);
    }
    */

    MainWindow w;
    if (parser.isSet(cloKey) || parser.isSet(cloSecret))
        w.setAccountKeySecret(parser.value(cloKey), parser.value(cloSecret));
    w.show();

    return app.exec();
}
