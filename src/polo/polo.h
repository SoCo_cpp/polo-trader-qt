#ifndef POLO_H
#define POLO_H

#include <QObject>
#include <QMutex>
#include <QMap>
#include <QList>
#include <QVariantMap>
#include <QVariantList>
#include <QDateTime>
#include <QUrl>
#include <QJsonArray>

class PoloWSS;
class QNetworkAccessManager;

class Polo : public QObject
{
    Q_OBJECT
public:

    typedef struct
    {
        QVariantMap details;
        QMap<qreal,qreal> asks;
        QMap<qreal,qreal> bids;
        QDateTime dt;
        unsigned long sequence;
    } OrderBook;

explicit Polo(QObject *parent = 0, QNetworkAccessManager* networkManager = 0);
    ~Polo();

    const QString& lastError() const;

    void setKeySecret(const QString& key, const QString& secret);
    void setUrls(const QUrl& urlTrade, const QUrl& urlPublic, const QUrl& urlWSS = QUrl());

    void autoOpenWSS(bool autoOpenEnabled = true);
    bool openWSS(const QUrl& urlWSS = QUrl());
    void closeWSS();

    bool subscribeTicker();
    bool unsubscribeTicker();

    bool subscribeOrderBook(const QString& currencyPair);
    bool unsubscribeOrderBook(const QString& currencyPair);

    bool requestBalances(bool tradable = true);
    bool requestSell(const QString& currencyPair, qreal rate, qreal amount);
    bool requestBuy(const QString& currencyPair, qreal rate, qreal amount);
    bool requestMoveOrder(unsigned long orderNumber, qreal rate, qreal amount = -1.0);
    bool requestCancelOrder(unsigned long orderNumber);
    bool requestOrderTrades(unsigned long orderNumber);
    bool requestTickers();
    bool requestChartData(const QString& currencyPair, const QDateTime& start, const QDateTime& end, int period);
    bool requestTradeHistory(const QString& currencyPair, const QDateTime& start, const QDateTime& end);
    bool requestOpenOrders(const QString& currencyPair);
    bool requestOrderBook(const QString& currencyPair, unsigned int depth);
    bool requestDepositAddresses();
    bool requestGenerateNewAddress(const QString& currency);
    bool requestWithdraw(const QString& currency, qreal amount, const QString& address);

    const QMap<QString,QVariantMap>& balances();
    QMutex* mxBalances();
    qreal balance(const QString& currency);
    qreal tradableBalance(const QString& currencyPair, const QString& currency);
    QDateTime dtBalances() const;

    const QMap<QString,QVariantList>& tickers();
    QMutex* mxTickers();
    QVariantList tickerValues(const QString& currencyPair);
    QDateTime dtTickers() const;
    const QStringList& tickerFieldNames() const;
    QList<unsigned int>& tickerIDFilter();

    const QList<QVariantMap>& chartData();
    QMutex* mxChartData();
    QVariantMap chartDataValues(unsigned int index);
    qreal chartDataValue(unsigned int index, const QString& field);
    QDateTime dtChartData() const;

    const QList<QVariantMap>& tradeOrders();
    QMutex* mxTradeOrders();
    QDateTime dtTradeOrders() const;

    const QMap<QString,OrderBook>& orderBooks();
    const OrderBook orderBook(const QString& currencyPair);
    QMutex* mxOrderBook();
    QDateTime dtOrderBook();

    const QMap<QString,QString>& depositAddresses();
    QMutex* mxDepositAddresses();
    QDateTime dtDepositAddresses() const;

    QString currencyPairByID(unsigned int pairID, const QString def = QString());

protected:
    QString lastError_;

    bool ownNetMan_;
    QNetworkAccessManager* netMan_;

    PoloWSS* wss_;
    bool autoOpenWSS_;
    QStringList tickerFieldNames_;

    QString key_;
    QString secret_;
    QUrl urlTrade_;
    QUrl urlPublic_;
    QUrl urlWSS_;

    QString lastRequestCurrency_;
    QString wssSubscribeCurrency_;

    QMap<QString,QVariantMap> balances_;
    QMutex mxBalances_;
    QDateTime dtBalances_;

    QMap<QString,QVariantList> tickers_;
    QMutex mxTickers_;
    QDateTime dtTickers_;
    QList<unsigned int> tickerIDFilter_;

    QList<QVariantMap> chartData_;
    QMutex mxChartData_;
    QDateTime dtChartData_;

    QList<QVariantMap> tradeOrders_;
    QMutex mxTradeOrders_;
    QDateTime dtTradeOrders_;

    QMap<QString,OrderBook> orderBooks_;
    QMutex mxOrderBook_;
    QDateTime dtOrderBook_;

    QMap<unsigned int,QString> currencyPairs_;
    QMutex mxCurrencyPairs_;

    QMap<QString,QString> depositAddresses_;
    QMutex mxDepositAddresses_;
    QDateTime dtDepositAddresses_;

signals:
    void success();
    void failed();
    void buysellmove(unsigned long orderNumber);
    void tradeOrdersUpdated();
    void balancesUpdated();
    void tickersUpdated();
    void chartDataUpdated();
    void orderBookUpdated(QString currencyPair);
    void depositAddressesUpdated();

public slots:
    void handleError();
    void handleBalances();
    void handleTradableBalances();
    void handleBuySellMove();
    void handleCancelOrder();
    void handleOrderTrades();
    void handleOpenOrdersAll();
    void handleTickers();
    void handleChartData();
    void handleOrderBook();
    void handleDepositAddresses();
    void handleGenerateNewAddress();
    void handleWithdraw();
    void handleWSSTicker(QJsonArray message);
    void handleWSSOrderBook(QJsonArray message);
    void handleWSSFail(QString errorMessage);
    void wssSubscribeTicker();
    void wssSubscribeOrderBook();
};

#endif // POLO_H
