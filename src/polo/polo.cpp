//----------------------

//#define NO_POLO_WSS // Disable support for PoloWSS
//----------------------
#include "polo.h"
#include "polohttp.h"
#include <QNetworkAccessManager>
#include <QMap>
#include <QMutexLocker>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <iterator>

#ifndef NO_POLO_WSS
#include "polowss.h"
#endif

Polo::Polo(QObject *parent /*= 0*/, QNetworkAccessManager* networkManager /*= 0*/) :  QObject(parent)
{
    autoOpenWSS_ = false;
    ownNetMan_ = (networkManager == 0);
    if (ownNetMan_)
        netMan_ = new QNetworkAccessManager();
    else
        netMan_ = networkManager;
    Q_ASSERT(netMan_);
#ifndef NO_POLO_WSS
    wss_ = new PoloWSS();
    Q_ASSERT(wss_);
    connect(wss_, SIGNAL(ticker(QJsonArray)), this, SLOT(handleWSSTicker(QJsonArray)));
    connect(wss_, SIGNAL(orderbook(QJsonArray)), this, SLOT(handleWSSOrderBook(QJsonArray)));
    connect(wss_, SIGNAL(failed(QString)), this, SLOT(handleWSSFail(QString)));
#endif
    tickerFieldNames_ << "id" << "last" << "lowestAsk" << "highestBid" << "percentChange" << "baseVolume" << "quoteVolume" << "isFrozen" << "high24hr" << "low24hr";
}

Polo::~Polo()
{
#ifndef NO_POLO_WSS
    if (wss_)
        {delete wss_;wss_ = 0;}
#endif
    if (ownNetMan_ && netMan_)
        {delete netMan_;netMan_ = 0;}
}

const QString& Polo::lastError() const
{
    return lastError_;
}

void Polo::setKeySecret(const QString& key, const QString& secret)
{
    key_ = key;
    secret_ = secret;
}

void Polo::setUrls(const QUrl& urlTrade, const QUrl& urlPublic, const QUrl& urlWSS /*= QUrl()*/)
{
    urlTrade_ = urlTrade;
    urlPublic_ = urlPublic;
    urlWSS_ = urlWSS;
}

const QMap<QString,QVariantMap>& Polo::balances()
{
    return balances_;
}

QMutex* Polo::mxBalances()
{
    return &mxBalances_;
}

QDateTime Polo::dtBalances() const
{
    return dtBalances_;
}

qreal Polo::balance(const QString& currency)
{
    QMutexLocker locker(&mxBalances_);
    return balances_["ALL"].value(currency, 0.0).toFloat();
}

qreal Polo::tradableBalance(const QString& currencyPair, const QString& currency)
{
    QMutexLocker locker(&mxBalances_);
    return balances_[currencyPair].value(currency, 0.0).toFloat();
}

const QMap<QString,QVariantList>& Polo::tickers()
{
    return tickers_;
}

QMutex* Polo::mxTickers()
{
    return &mxTickers_;
}

QVariantList Polo::tickerValues(const QString& currencyPair)
{
    QMutexLocker locker(&mxTickers_);
    return tickers_[currencyPair];
}

QDateTime Polo::dtTickers() const
{
    return dtTickers_;
}

const QStringList& Polo::tickerFieldNames() const
{
    return tickerFieldNames_;
}

QList<unsigned int>& Polo::tickerIDFilter()
{
    return tickerIDFilter_;
}

const QList<QVariantMap>& Polo::chartData()
{
    return chartData_;
}

QMutex* Polo::mxChartData()
{
    return &mxChartData_;
}

QVariantMap Polo::chartDataValues(unsigned int index)
{
    QMutexLocker locker(&mxChartData_);
    if (index >= (unsigned int)chartData_.size())
        return QVariantMap();
    return chartData_[index];
}

qreal Polo::chartDataValue(unsigned int index, const QString& field)
{
    QMutexLocker locker(&mxChartData_);
    if (index >= (unsigned int)chartData_.size())
        return 0.0;
    if (!chartData_[index].contains(field))
        return 0.0;
    if (!chartData_[index].value(field).canConvert(QMetaType::Float))
        return 0.0;
    return chartData_[index].value(field).toFloat();
}

QDateTime Polo::dtChartData() const
{
    return dtChartData_;
}

const QList<QVariantMap>& Polo::tradeOrders()
{
    return tradeOrders_;
}

QMutex* Polo::mxTradeOrders()
{
    return &mxTradeOrders_;
}

QDateTime Polo::dtTradeOrders() const
{
    return dtTradeOrders_;
}

const QMap<QString,Polo::OrderBook>& Polo::orderBooks()
{
    return orderBooks_;
}

const Polo::OrderBook Polo::orderBook(const QString& currencyPair)
{
    return orderBooks_.value(currencyPair);
}

QMutex* Polo::mxOrderBook()
{
    return &mxOrderBook_;
}

QDateTime Polo::dtOrderBook()
{
    return dtOrderBook_;
}

const QMap<QString,QString>& Polo::depositAddresses()
{
    return depositAddresses_;
}

QMutex* Polo::mxDepositAddresses()
{
    return &mxDepositAddresses_;
}

QDateTime Polo::dtDepositAddresses() const
{
    return dtDepositAddresses_;
}

void Polo::autoOpenWSS(bool autoOpenEnabled /*= true*/)
{
    autoOpenWSS_ = autoOpenEnabled;
}

bool Polo::openWSS(const QUrl& urlWSS /*= QUrl()*/)
{
#ifndef NO_POLO_WSS
    Q_ASSERT(wss_);
    QUrl url = urlWSS;
    if (url.isEmpty())
        url = urlWSS_;
    if (!wss_->isOpen())
        if (!wss_->open(url)) // open checks url for valid
        {
            lastError_ = QString("openWSS WSS open failed: %1").arg(wss_->lastError());
            return false;
        }
    return true;
#else
    Q_UNUSED(urlWSS);
    lastError_ = "openWSS PoloWSS not emabled at compile time";
    return false;
#endif
}

void Polo::closeWSS()
{
#ifndef NO_POLO_WSS
    Q_ASSERT(wss_);
    wss_->close();
#endif
}

bool Polo::subscribeTicker()
{
#ifndef NO_POLO_WSS
    Q_ASSERT(wss_);
    if (!wss_->isOpen())
    {
        if (!autoOpenWSS_)
        {
            lastError_ = "subscribeTicker WSS not open";
            return false;
        }
        connect(wss_, SIGNAL(connected()), this, SLOT(wssSubscribeTicker()));
        if (!wss_->open(urlWSS_)) // open checks url for valid
        {
            lastError_ = QString("subscribeTicker WSS open failed: %1").arg(wss_->lastError());
            return false;
        }
        return true;
    }
    wss_->subscribeTicker();
    return true;
#else
    lastError_ = "subscribeTicker PoloWSS not emabled at compile time";
    return false;
#endif
}

bool Polo::unsubscribeTicker()
{
#ifndef NO_POLO_WSS
    Q_ASSERT(wss_);
    if (!wss_->isOpen())
    {
        lastError_ = "unsubscribeTicker WSS not opened";
        return false;
    }
    wss_->unsubscribeTicker();
    return true;
#else
    lastError_ = "unsubscribeTicker PoloWSS not emabled at compile time";
    return false;
#endif
}

bool Polo::subscribeOrderBook(const QString& currencyPair)
{
#ifndef NO_POLO_WSS
    Q_ASSERT(wss_);
    if (!wss_->isOpen())
    {
        if (!autoOpenWSS_)
        {
            lastError_ = "subscribeOrderBook WSS not open";
            return false;
        }
        wssSubscribeCurrency_ = currencyPair;
        connect(wss_, SIGNAL(connected()), this, SLOT(wssSubscribeOrderBook()));
        if (!wss_->open(urlWSS_)) // open checks url for valid
        {
            lastError_ = QString("subscribeOrderBook '%1' WSS open failed: %2").arg(currencyPair).arg(wss_->lastError());
            return false;
        }
        return true;
    }
    wss_->subscribeOrderBook(currencyPair);
    return true;
#else
    Q_UNUSED(currencyPair);
    lastError_ = "subscribeOrderBook PoloWSS not emabled at compile time";
    return false;
#endif
}

bool Polo::unsubscribeOrderBook(const QString& currencyPair)
{
#ifndef NO_POLO_WSS
    Q_ASSERT(wss_);
    if (!wss_->isOpen())
    {
        lastError_ = QString("unsubscribeOrderBook '%1' WSS not opened").arg(currencyPair);
        return false;
    }
    wss_->unsubscribeOrderBook(currencyPair);
    return true;
#else
    Q_UNUSED(currencyPair);
    lastError_ = "unsubscribeOrderBook PoloWSS not emabled at compile time";
    return false;
#endif
}

bool Polo::requestBalances(bool tradable /*= true*/)
{
    PoloHTTP* http = new PoloHTTP(0/*parent*/, netMan_);
    Q_ASSERT(http);
    http->setKeySecret(key_, secret_);
    http->setUrls(urlTrade_, urlPublic_);
    QMap<QString,QString> req;
    connect(http, SIGNAL(failed()), this, SLOT(handleError()));
    if (tradable)
    {
        disconnect(http, SIGNAL(finished()), this, SLOT(handleBalances()));
        connect(http, SIGNAL(finished()), this, SLOT(handleTradableBalances()));
        req["command"] = "returnTradableBalances";
    }
    else
    {
        disconnect(http, SIGNAL(finished()), this, SLOT(handleTradableBalances()));
        connect(http, SIGNAL(finished()), this, SLOT(handleBalances()));
        req["command"] = "returnBalances";
    }
    if (!http->requestTrade(req))
    {
        lastError_ = QString("requestBalances http requestTrade failed: %1").arg(http->lastError());
        http->deleteLater();
        return false;
    }
    qDebug() << "Polo::requestBalances done";
    return  true;
}

bool Polo::requestSell(const QString& currencyPair, qreal rate, qreal amount)
{
    PoloHTTP* http = new PoloHTTP(0/*parent*/, netMan_);
    Q_ASSERT(http);
    http->setKeySecret(key_, secret_);
    http->setUrls(urlTrade_, urlPublic_);
    QMap<QString,QString> req;
    req["command"] = "sell";
    req["currencyPair"] = currencyPair;
    req["rate"] = QString::number(rate, 'f'/*plain Format*/, 8/*precision*/);
    req["amount"] = QString::number(amount, 'f'/*plain Format*/, 8/*precision*/);
    connect(http, SIGNAL(finished()), this, SLOT(handleBuySellMove()));
    connect(http, SIGNAL(failed()), this, SLOT(handleError()));
    if (!http->requestTrade(req))
    {
        lastError_ = QString("requestSell http requestTrade failed: %1").arg(http->lastError());
        http->deleteLater();
        return false;
    }
    return  true;
}

bool Polo::requestBuy(const QString& currencyPair, qreal rate, qreal amount)
{
    PoloHTTP* http = new PoloHTTP(0/*parent*/, netMan_);
    Q_ASSERT(http);
    http->setKeySecret(key_, secret_);
    http->setUrls(urlTrade_, urlPublic_);
    QMap<QString,QString> req;
    req["command"] = "buy";
    req["currencyPair"] = currencyPair;
    req["rate"] = QString::number(rate, 'f'/*plain Format*/, 8/*precision*/);
    req["amount"] = QString::number(amount, 'f'/*plain Format*/, 8/*precision*/);
    connect(http, SIGNAL(finished()), this, SLOT(handleBuySellMove()));
    connect(http, SIGNAL(failed()), this, SLOT(handleError()));
    if (!http->requestTrade(req))
    {
        lastError_ = QString("requestBuy http requestTrade failed: %1").arg(http->lastError());
        http->deleteLater();
        return false;
    }
    return  true;
}

bool Polo::requestMoveOrder(unsigned long orderNumber, qreal rate, qreal amount /*= -1.0*/)
{
    PoloHTTP* http = new PoloHTTP(0/*parent*/, netMan_);
    Q_ASSERT(http);
    http->setKeySecret(key_, secret_);
    http->setUrls(urlTrade_, urlPublic_);
    QMap<QString,QString> req;
    req["command"] = "moveOrder";
    req["orderNumber"] = QString::number(orderNumber);
    req["rate"] = QString::number(rate, 'f'/*plain Format*/, 8/*precision*/);
    if (amount != -1.0)
        req["amount"] = QString::number(amount, 'f'/*plain Format*/, 8/*precision*/);
    connect(http, SIGNAL(finished()), this, SLOT(handleBuySellMove()));
    connect(http, SIGNAL(failed()), this, SLOT(handleError()));
    if (!http->requestTrade(req))
    {
        lastError_ = QString("requestMoveOrder http requestTrade failed: %1").arg(http->lastError());
        http->deleteLater();
        return false;
    }
    return  true;
}

bool Polo::requestCancelOrder(unsigned long orderNumber)
{
    PoloHTTP* http = new PoloHTTP(0/*parent*/, netMan_);
    Q_ASSERT(http);
    http->setKeySecret(key_, secret_);
    http->setUrls(urlTrade_, urlPublic_);
    QMap<QString,QString> req;
    req["command"] = "cancelOrder";
    req["orderNumber"] = QString::number(orderNumber);
    connect(http, SIGNAL(finished()), this, SLOT(handleCancelOrder()));
    connect(http, SIGNAL(failed()), this, SLOT(handleError()));
    if (!http->requestTrade(req))
    {
        lastError_ = QString("requestCancelOrder http requestTrade failed: %1").arg(http->lastError());
        http->deleteLater();
        return false;
    }
    return  true;
}

bool Polo::requestOrderTrades(unsigned long orderNumber)
{
    PoloHTTP* http = new PoloHTTP(0/*parent*/, netMan_);
    Q_ASSERT(http);
    http->setKeySecret(key_, secret_);
    http->setUrls(urlTrade_, urlPublic_);
    QMap<QString,QString> req;
    req["command"] = "returnOrderTrades";
    req["orderNumber"] = QString::number(orderNumber);
    connect(http, SIGNAL(finished()), this, SLOT(handleOrderTrades()));
    connect(http, SIGNAL(failed()), this, SLOT(handleError()));
    if (!http->requestTrade(req))
    {
        lastError_ = QString("requestOrderTrades http requestTrade failed: %1").arg(http->lastError());
        http->deleteLater();
        return false;
    }
    return  true;
}

bool Polo::requestTickers()
{
    PoloHTTP* http = new PoloHTTP(0/*parent*/, netMan_);
    Q_ASSERT(http);
    http->setKeySecret(key_, secret_);
    http->setUrls(urlTrade_, urlPublic_);
    QMap<QString,QString> req;
    req["command"] = "returnTicker";
    connect(http, SIGNAL(finished()), this, SLOT(handleTickers()));
    connect(http, SIGNAL(failed()), this, SLOT(handleError()));
    if (!http->requestPublic(req))
    {
        lastError_ = QString("requestTickers http requestPublic failed: %1").arg(http->lastError());
        http->deleteLater();
        return false;
    }
    return  true;
}

// (candlestick period in seconds; valid values are 300, 900, 1800, 7200, 14400, and 86400)
bool Polo::requestChartData(const QString& currencyPair, const QDateTime& start, const QDateTime& end, int period)
{
    PoloHTTP* http = new PoloHTTP(0/*parent*/, netMan_);
    Q_ASSERT(http);
    http->setKeySecret(key_, secret_);
    http->setUrls(urlTrade_, urlPublic_);
    QMap<QString,QString> req;
    req["command"] = "returnChartData";
    req["currencyPair"] = currencyPair;
    req["start"] = QString::number(start.toTime_t());
    req["end"] = QString::number(end.toTime_t());
    req["period"] = QString::number(period);
    qDebug() << req;
    connect(http, SIGNAL(finished()), this, SLOT(handleChartData()));
    connect(http, SIGNAL(failed()), this, SLOT(handleError()));
    if (!http->requestPublic(req))
    {
        lastError_ = QString("requestChartData http requestPublic failed: %1").arg(http->lastError());
        http->deleteLater();
        return false;
    }
    return  true;
}

bool Polo::requestTradeHistory(const QString& currencyPair, const QDateTime& start, const QDateTime& end)
{
    PoloHTTP* http = new PoloHTTP(0/*parent*/, netMan_);
    Q_ASSERT(http);
    http->setKeySecret(key_, secret_);
    http->setUrls(urlTrade_, urlPublic_);
    QMap<QString,QString> req;
    req["command"] = "returnTradeHistory";
    req["currencyPair"] = currencyPair;
    req["start"] = QString::number(start.toTime_t());
    req["end"] = QString::number(end.toTime_t());
    qDebug() << req;
    connect(http, SIGNAL(finished()), this, SLOT(handleOrderTrades()));
    connect(http, SIGNAL(failed()), this, SLOT(handleError()));
    if (!http->requestPublic(req))
    {
        lastError_ = QString("requestTradeHistory http requestPublic failed: %1").arg(http->lastError());
        http->deleteLater();
        return false;
    }
    return  true;
}

//  currencyPair additionally can be 'all'
bool Polo::requestOpenOrders(const QString& currencyPair)
{
    PoloHTTP* http = new PoloHTTP(0/*parent*/, netMan_);
    Q_ASSERT(http);
    http->setKeySecret(key_, secret_);
    http->setUrls(urlTrade_, urlPublic_);
    QMap<QString,QString> req;
    req["command"] = "returnOpenOrders";
    req["currencyPair"] = currencyPair;
    if (currencyPair == "all")
        connect(http, SIGNAL(finished()), this, SLOT(handleOpenOrdersAll()));
    else
        connect(http, SIGNAL(finished()), this, SLOT(handleOrderTrades()));
    connect(http, SIGNAL(failed()), this, SLOT(handleError()));
    if (!http->requestTrade(req))
    {
        lastError_ = QString("requestOpenOrders http requestTrade failed: %1").arg(http->lastError());
        http->deleteLater();
        return false;
    }
    return  true;
}

bool Polo::requestOrderBook(const QString& currencyPair, unsigned int depth)
{
    if (currencyPair == "all")
    {
        lastError_ = "requestOrderBook 'all' currencyPair not currently supported by Polo class.";
        return false;
    }
    PoloHTTP* http = new PoloHTTP(0/*parent*/, netMan_);
    Q_ASSERT(http);
    http->setKeySecret(key_, secret_);
    http->setUrls(urlTrade_, urlPublic_);
    QMap<QString,QString> req;
    req["command"] = "returnOrderBook";
    req["currencyPair"] = currencyPair;
    req["depth"] = QString::number(depth);
    connect(http, SIGNAL(finished()), this, SLOT(handleOrderBook()));
    connect(http, SIGNAL(failed()), this, SLOT(handleError()));
    if (!http->requestPublic(req))
    {
        lastError_ = QString("requestOrderBook http requestPublic failed: %1").arg(http->lastError());
        http->deleteLater();
        return false;
    }
    return  true;
}

bool Polo::requestDepositAddresses()
{
    PoloHTTP* http = new PoloHTTP(0/*parent*/, netMan_);
    Q_ASSERT(http);
    http->setKeySecret(key_, secret_);
    http->setUrls(urlTrade_, urlPublic_);
    QMap<QString,QString> req;
    req["command"] = "returnDepositAddresses";
    connect(http, SIGNAL(finished()), this, SLOT(handleDepositAddresses()));
    connect(http, SIGNAL(failed()), this, SLOT(handleError()));
    if (!http->requestTrade(req))
    {
        lastError_ = QString("requestDepositAddresses http requestTrade failed: %1").arg(http->lastError());
        http->deleteLater();
        return false;
    }
    return  true;
}

bool Polo::requestGenerateNewAddress(const QString& currency)
{
    PoloHTTP* http = new PoloHTTP(0/*parent*/, netMan_);
    Q_ASSERT(http);
    http->setKeySecret(key_, secret_);
    http->setUrls(urlTrade_, urlPublic_);
    QMap<QString,QString> req;
    req["command"] = "generateNewAddress";
    req["currency"] = currency;
    lastRequestCurrency_ = currency;
    connect(http, SIGNAL(finished()), this, SLOT(handleGenerateNewAddress()));
    connect(http, SIGNAL(failed()), this, SLOT(handleError()));
    if (!http->requestTrade(req))
    {
        lastError_ = QString("requestGenerateNewAddress http requestTrade failed: %1").arg(http->lastError());
        http->deleteLater();
        return false;
    }
    return  true;
}

bool Polo::requestWithdraw(const QString& currency, qreal amount, const QString& address)
{
    PoloHTTP* http = new PoloHTTP(0/*parent*/, netMan_);
    Q_ASSERT(http);
    http->setKeySecret(key_, secret_);
    http->setUrls(urlTrade_, urlPublic_);
    QMap<QString,QString> req;
    req["command"] = "withdraw";
    req["currency"] = currency;
    req["amount"] = QString::number(amount);
    req["address"] = address;
    connect(http, SIGNAL(finished()), this, SLOT(handleWithdraw()));
    connect(http, SIGNAL(failed()), this, SLOT(handleError()));
    if (!http->requestTrade(req))
    {
        lastError_ = QString("requestWithdraw http requestTrade failed: %1").arg(http->lastError());
        http->deleteLater();
        return false;
    }
    return  true;
}

void Polo::handleError()
{
    PoloHTTP* http = qobject_cast<PoloHTTP*>(sender());
    Q_ASSERT(http);
    lastError_ = http->lastError();
    http->deleteLater();
    emit failed();
}

void Polo::handleBalances()
{
    PoloHTTP* http = qobject_cast<PoloHTTP*>(sender());
    Q_ASSERT(http);
    {
        if (!http->jsonReply()->isObject())
        {
            qDebug("Polo::handleBalances validate jsonReply isObject failed");
            http->deleteLater();
            return;
        }
        QMutexLocker locker(&mxBalances_);
        balances_["ALL"] = http->jsonReply()->object().toVariantMap();
        qDebug() << "handleBalances balances size: " << balances_.size();
        dtBalances_ = QDateTime::currentDateTime();
        http->deleteLater();
    }
    qDebug() << "handleBalances done";
    emit balancesUpdated();
}

void Polo::handleTradableBalances()
{
    PoloHTTP* http = qobject_cast<PoloHTTP*>(sender());
    Q_ASSERT(http);
    {
        if (!http->jsonReply()->isObject())
        {
            qDebug("Polo::handleTradableBalances validate jsonReply isObject failed");
            http->deleteLater();
            return;
        }
        QMutexLocker locker(&mxBalances_);
        foreach (const QString& key, http->jsonReply()->object().keys())
            balances_[key] = http->jsonReply()->object()[key].toObject().toVariantMap();
        qDebug() << "handleTradableBalances balances size: " << balances_.size();
        dtBalances_ = QDateTime::currentDateTime();
        http->deleteLater();
    }
    qDebug() << "handleTradableBalances done";
    emit balancesUpdated();
}

void Polo::handleBuySellMove()
{
    bool wasFail = false;
    unsigned long orderNumber;
    PoloHTTP* http = qobject_cast<PoloHTTP*>(sender());
    Q_ASSERT(http);
    {
        if (!http->jsonReply()->isObject())
        {
            qDebug("Polo::handleBuySellMove validate jsonReply isObject failed");
            http->deleteLater();
            return;
        }
        if (!http->jsonReply()->object().contains("orderNumber") || !http->jsonReply()->object().contains("resultingTrades"))
        {
            qDebug("Polo::handleBuySellMove validate jsonReply expected fields orderNumber and resultingTrades failed");
            http->deleteLater();
            return;
        }
        if (http->jsonReply()->object().contains("success"))
            wasFail = (http->jsonReply()->object().value("success").toInt(0) == 0);
        QMutexLocker locker(&mxTradeOrders_);
        orderNumber = http->jsonReply()->object().value("orderNumber").toString().toLong();
        if (!http->jsonReply()->object().value("resultingTrades").isArray())
        {
            QJsonArray array = http->jsonReply()->object().value("resultingTrades").toArray();
            for (int i = 0;i < array.size();i++)
                tradeOrders_.append(array.at(i).toObject().toVariantMap());
        }
        else if (!http->jsonReply()->object().value("resultingTrades").isObject())
        {
            QJsonObject object = http->jsonReply()->object().value("resultingTrades").toObject();
            foreach (const QString& key, object.keys())
            {
                if (object.value(key).isArray())
                {
                    qDebug("Polo::handleBuySellMove validate jsonReply resultingTrades object field is array failed");
                    return;
                }
                QJsonArray array = object.value(key).toArray();
                for (int i = 0;i < array.size();i++)
                {
                    QVariantMap data = array.at(i).toObject().toVariantMap();
                    data.insert("currencyPair", key);
                    tradeOrders_.append(data);
                }
            }
        }
        else
        {
            qDebug("Polo::handleBuySellMove validate jsonReply resultingTrades isArray or isObject failed");
            return;
        }
        dtTradeOrders_ = QDateTime::currentDateTime();
        http->deleteLater();
    }
    if (wasFail)
        emit failed();
    else
        emit buysellmove(orderNumber);
    emit tradeOrdersUpdated();
}

void Polo::handleCancelOrder()
{
    PoloHTTP* http = qobject_cast<PoloHTTP*>(sender());
    Q_ASSERT(http);
    if (!http->jsonReply()->isObject())
    {
        qDebug("Polo::handleCancelOrder validate jsonReply isObject failed");
        http->deleteLater();
        return;
    }
    if (http->jsonReply()->object().value("success").toInt(0) != 0)
        emit success();
    else
        emit failed();
    http->deleteLater();
}

void Polo::handleOrderTrades()
{
    PoloHTTP* http = qobject_cast<PoloHTTP*>(sender());
    Q_ASSERT(http);
    {
        if (!http->jsonReply()->isArray())
        {
            qDebug("Polo::handleOrderTrades validate jsonReply isArray failed");
            http->deleteLater();
            return;
        }
        QMutexLocker locker(&mxTradeOrders_);
        for (int i = 0;i < http->jsonReply()->array().size();i++)
            tradeOrders_.append(http->jsonReply()->array().at(i).toObject().toVariantMap());
        dtTradeOrders_ = QDateTime::currentDateTime();
        http->deleteLater();
    }
    emit tradeOrdersUpdated();
}

void Polo::handleOpenOrdersAll()
{
    PoloHTTP* http = qobject_cast<PoloHTTP*>(sender());
    Q_ASSERT(http);
    {
        if (!http->jsonReply()->isObject())
        {
            qDebug("Polo::handleOpenOrdersAll validate jsonReply isObject failed");
            http->deleteLater();
            return;
        }
        QMutexLocker locker(&mxTradeOrders_);
        foreach (const QString& key, http->jsonReply()->object().keys())
        {
            if (http->jsonReply()->object()[key].isArray())
            {
                QJsonArray array = http->jsonReply()->object()[key].toArray();
                for (int i = 0;i < array.size();i++)
                {
                    QVariantMap data = array.at(i).toObject().toVariantMap();
                    data.insert("currencyPair", key);
                    tradeOrders_.append(data);
                }
            }
        }
        dtTradeOrders_ = QDateTime::currentDateTime();
        http->deleteLater();
    }
    emit tradeOrdersUpdated();
}

void Polo::handleTickers()
{
    PoloHTTP* http = qobject_cast<PoloHTTP*>(sender());
    Q_ASSERT(http);
    {
        if (!http->jsonReply()->isObject())
        {
            qDebug("Polo::handleTickers validate jsonReply isObject failed");
            http->deleteLater();
            return;
        }
        QMutexLocker locker(&mxTickers_);
        QMutexLocker cpLocker(&mxCurrencyPairs_);
        foreach (const QString& key, http->jsonReply()->object().keys())
        {
            QJsonObject data = http->jsonReply()->object()[key].toObject();
            unsigned int id = data.value("id").toInt();
            if (tickerIDFilter_.size() == 0 || tickerIDFilter_.contains(id))
            {
                QVariantMap datamap = data.toVariantMap();
                QVariantList datalist;
                // dont use datamap.keys or values will be sorted
                foreach (const QString& fieldName, tickerFieldNames_)
                    datalist.append(datamap[fieldName]);
                tickers_[key] = datalist;
            }
            if (!currencyPairs_.contains(id))
                currencyPairs_.insert(id, key);
        }
        dtTickers_ = QDateTime::currentDateTime();
        http->deleteLater();
    }
    emit tickersUpdated();
}

void Polo::handleChartData()
{
    PoloHTTP* http = qobject_cast<PoloHTTP*>(sender());
    Q_ASSERT(http);
    {
        if (!http->jsonReply()->isArray())
        {
            qDebug("Polo::handleChartData validate jsonReply isArray failed");
            http->deleteLater();
            return;
        }
        QMutexLocker locker(&mxChartData_);
        chartData_.clear();
        for (int i = 0;i < http->jsonReply()->array().size();i++)
            chartData_.append(http->jsonReply()->array().at(i).toObject().toVariantMap());
        dtChartData_ = QDateTime::currentDateTime();
        http->deleteLater();
    }
    emit chartDataUpdated();
}

void Polo::handleOrderBook()
{
    PoloHTTP* http = qobject_cast<PoloHTTP*>(sender());
    Q_ASSERT(http);
    {
        if (!http->jsonReply()->isObject())
        {
            qDebug("Polo::handleOrderBook validate jsonReply isObject failed");
            http->deleteLater();
            return;
        }
        QMutexLocker locker(&mxOrderBook_);

        orderBooks_[lastRequestCurrency_].asks.clear();
        orderBooks_[lastRequestCurrency_].bids.clear();
        orderBooks_[lastRequestCurrency_].details.clear();

        foreach (const QString& key, http->jsonReply()->object().keys())
        {
            if (key == "asks" || key == "bids")
            {
                if (!http->jsonReply()->object().value(key).isArray())
                {
                    qDebug("Polo::handleOrderBook validate jsonReply isArray failed");
                    http->deleteLater();
                    return;
                }
                QJsonArray array = http->jsonReply()->object().value(key).toArray();
                for (int i = 0;i < array.size();i++)
                {

                    if (!array.at(i).isArray())
                    {
                        qDebug("Polo::handleOrderBook validate jsonReply isArray values failed");
                        http->deleteLater();
                        return;
                    }
                    QJsonArray arrayValues = array.at(i).toArray();

                    if (arrayValues.size() != 2)
                    {
                        qDebug("Polo::handleOrderBook validate jsonReply values array size failed");
                        http->deleteLater();
                        return;
                    }
                    if (!arrayValues[0].isString() || !arrayValues[1].isDouble())
                    {
                        qDebug("Polo::handleOrderBook validate jsonReply value array data types failed");
                        http->deleteLater();
                        return;
                    }
                    // arrayValues[0]'s are string values! arrayValues[1]'s are not!
                    // They QJsonValue will not toDouble the string values.
                    // Use the QJsonValue's toString, then you can toDouble from the QString.
                    if (key == "asks")
                        orderBooks_[lastRequestCurrency_].asks.insert(arrayValues[0].toString().toDouble(), arrayValues[1].toDouble());
                    else
                        orderBooks_[lastRequestCurrency_].bids.insert(arrayValues[0].toString().toDouble(), arrayValues[1].toDouble());
                }
            }
            else
                orderBooks_[lastRequestCurrency_].details.insert(key, http->jsonReply()->object().value(key).toVariant());
        }
        dtOrderBook_ = QDateTime::currentDateTime();
        http->deleteLater();
    }
    emit orderBookUpdated(lastRequestCurrency_);
}

void Polo::handleDepositAddresses()
{
    PoloHTTP* http = qobject_cast<PoloHTTP*>(sender());
    Q_ASSERT(http);
    {
        if (!http->jsonReply()->isObject())
        {
            qDebug("Polo::handleDepositAddresses validate jsonReply isObject failed");
            http->deleteLater();
            return;
        }
        QMutexLocker locker(&mxDepositAddresses_);
        depositAddresses_.clear();
        foreach (const QString& key, http->jsonReply()->object().keys())
            depositAddresses_.insert(key, http->jsonReply()->object().value(key).toString());
        dtDepositAddresses_ = QDateTime::currentDateTime();
        http->deleteLater();
    }
    emit depositAddressesUpdated();
}

void Polo::handleGenerateNewAddress()
{
    PoloHTTP* http = qobject_cast<PoloHTTP*>(sender());
    Q_ASSERT(http);
    {
        if (!http->jsonReply()->isObject())
        {
            qDebug("Polo::handleDepositAddresses validate jsonReply isObject failed");
            http->deleteLater();
            return;
        }
        if (!http->jsonReply()->object().contains("success") || http->jsonReply()->object().value("success").toInt() == 0 || !http->jsonReply()->object().contains("response") )
        {
            lastError_ = "Generate new address not successful";
            emit failed();
            http->deleteLater();
            return;
        }
        QMutexLocker locker(&mxDepositAddresses_);
        depositAddresses_.clear();
        depositAddresses_.insert(lastRequestCurrency_, http->jsonReply()->object().value("response").toString());
        dtDepositAddresses_ = QDateTime::currentDateTime();
        http->deleteLater();
    }
    emit depositAddressesUpdated();
}

void Polo::handleWithdraw()
{
    PoloHTTP* http = qobject_cast<PoloHTTP*>(sender());
    Q_ASSERT(http);
    {
        if (!http->jsonReply()->isObject())
        {
            qDebug("Polo::handleWithdraw validate jsonReply isObject failed");
            http->deleteLater();
            return;
        }
        if (!http->jsonReply()->object().contains("response"))
        {
            lastError_ = "Withdraw not successful";
            emit failed();
            http->deleteLater();
            return;
        }
        QMutexLocker locker(&mxDepositAddresses_);
        lastError_ = http->jsonReply()->object().value("response").toString();
        depositAddresses_.clear();
        depositAddresses_.insert(lastRequestCurrency_, http->jsonReply()->object().value("response").toString());
        dtDepositAddresses_ = QDateTime::currentDateTime();
        http->deleteLater();
    }
    emit success();
}

void Polo::handleWSSTicker(QJsonArray message)
{
    if (message.size() != 3)
    {
        qDebug("Polo::handleWSSTicker validate field count failed");
        return;
    }
    if (!message.at(2).isArray())
    {
        qDebug("Polo::handleWSSTicker validate data array failed");
        return;
    }
    QJsonArray ticker = message.at(2).toArray();
    {
        QMutexLocker locker(&mxTickers_);
        QVariantList tickerData;
        //qDebug() << "ticker (" << ticker.size() << ") " << ticker;
        if (ticker.size() < 2 || ticker.size() > (1 + tickerFieldNames_.size()))
        {
            qDebug() << "Polo::handleWSSTicker validate ticker field count failed";
            return;
        }
        unsigned int id = ticker.at(0).toInt();
        if (tickerIDFilter_.size() == 0 || tickerIDFilter_.contains(id))
        {
            tickerData.append(id);
            for (int i = 1/*skip 0*/;i < ticker.size();i++)
            {
                if (i == 7/*isfrozen int*/)
                    tickerData.append(ticker.at(i).toInt());
                else
                    tickerData.append(ticker.at(i).toString().toDouble());
            }
            QMutexLocker cpLocker(&mxCurrencyPairs_);
            QString currencyPair = currencyPairs_[id];
            if (currencyPair.isEmpty())
                currencyPair = QString::number(id);
            tickers_[currencyPair] = tickerData;
        }
    }
    emit tickersUpdated();
}

void Polo::handleWSSOrderBook(QJsonArray message)
{
    if (message.size() != 3)
    {
        qDebug("Polo::handleWSSOrderBook main message size invalid: %d", message.size());
        return;
    }
    if (!message.at(0).isDouble() || !message.at(1).isDouble() || !message.at(2).isArray())
    {
        qDebug() << "Polo::handleWSSOrderBook main field type invalid: " <<  message;
        return;
    }
    unsigned int currencyID = (unsigned int)message.at(0).toDouble();
    unsigned long sequence = (unsigned long)message.at(1).toDouble();
    QJsonArray msgList = message.at(2).toArray();
    //qDebug() << "handle order book, currencyID: " << currencyID << ", sequence: " << sequence << ", msgList size: " << msgList.size();
    for (int m = 0;m < msgList.size();m++)
    {
        if (!msgList.at(m).isArray())
        {
            qDebug() << "Polo::handleWSSOrderBook msg type invalid: " <<  msgList.at(m);
            return;
        }
        QJsonArray msg = msgList.at(m).toArray();
        if (!msg.at(0).isString())
        {
            qDebug() << "Polo::handleWSSOrderBook msg type invalid: " <<  msgList.at(m);
            return;
        }
        QString msgType = msg.at(0).toString();
        if (msgType == "i")
        {
            //qDebug() << "handling 'i'";
            if (msg.size() != 2 || !msg.at(1).isObject())
            {
                qDebug() << "Polo::handleWSSOrderBook msg i invalid: " <<  msg;
                return;
            }
            QJsonObject msgData = msg.at(1).toObject();
            if (!msgData.contains("currencyPair") || !msgData.contains("orderBook") || !msgData.value("currencyPair").isString() || !msgData.value("orderBook").isArray())
            {
                qDebug() << "Polo::handleWSSOrderBook msgData verify failed: " <<  msgData;
                return;
            }
            QString currencyPair = msgData.value("currencyPair").toString();
            if (sequence > orderBooks_[currencyPair].sequence || (orderBooks_[currencyPair].sequence - sequence) > 0xfff000 /*rollover*/)
            {
                QJsonArray orderBook = msgData.value("orderBook").toArray();
                if (orderBook.size() != 2 || !orderBook.at(0).isObject() || !orderBook.at(1).isObject())// sell/buy
                {
                    qDebug() << "Polo::handleWSSOrderBook msgData verify orderbook failed: " <<  msgData;
                    return;
                }
                QJsonObject asks = orderBook.at(0).toObject();
                QJsonObject bids = orderBook.at(1).toObject();

                {
                    QMutexLocker locker(&mxOrderBook_);

                    orderBooks_[currencyPair].asks.clear();
                    foreach (const QString& key, asks.keys())
                        orderBooks_[currencyPair].asks.insert(key.toDouble(), asks.value(key).toString().toDouble());

                    orderBooks_[currencyPair].bids.clear();
                    foreach (const QString& key, bids.keys())
                        orderBooks_[currencyPair].bids.insert(key.toDouble(), bids.value(key).toString().toDouble());

                    orderBooks_[currencyPair].sequence = sequence;
                    orderBooks_[currencyPair].dt = QDateTime::currentDateTime();

                    dtOrderBook_ = QDateTime::currentDateTime();

                    QMutexLocker cpLocker(&mxCurrencyPairs_);
                    currencyPairs_[currencyID] = currencyPair;
                }

                emit orderBookUpdated(currencyPair);
            }
        } // i
        else if (msgType == "o" || msgType == "t")
        {
            //qDebug() << "handling 'o'/'t'";
            if (msgType == "o")
            {
                //qDebug() << "handling 'o'";
                QString currencyPair = currencyPairByID(currencyID);
                if (msg.size() != 4 || !msg.at(1).isDouble() || !msg.at(2).isString() || !msg.at(3).isString())
                {
                    qDebug() << "Polo::handleWSSOrderBook msg o invalid: " <<  msg;
                    return;
                }
                bool isAsk = (msg.at(1).toDouble() == 0 ? true : false);
                qreal value = msg.at(2).toString().toDouble();
                qreal amount = msg.at(3).toString().toDouble();

                {
                    QMutexLocker locker(&mxOrderBook_);
                    QMap<qreal,qreal>& list = (isAsk ? orderBooks_[currencyPair].asks : orderBooks_[currencyPair].bids);
                    if (amount == 0)
                        list.remove(value);
                    else
                        list.insert(value, amount);
                    orderBooks_[currencyPair].dt = QDateTime::currentDateTime();
                    dtOrderBook_ = QDateTime::currentDateTime();
                }

                emit orderBookUpdated(currencyPair);
            }
            else // msgType == "t"
            {
                //qDebug() << "handling 't'";
                if (msg.size() != 6 || !msg.at(1).isString() || !msg.at(2).isDouble() || !msg.at(3).isString() || !msg.at(4).isString() || !msg.at(5).isDouble())
                {
                    qDebug() << "Polo::handleWSSOrderBook msg t invalid: " <<  msg;
                    return;
                }
                {
                    QMutexLocker locker(&mxTradeOrders_);
                    QVariantMap data;
                    data["date"]            = QDateTime::currentDateTime();
                    data["currencyID"]    = currencyID;
                    data["tradeID"]         = (int)msg.at(1).toString().toUInt();
                    data["type"]            = msg.at(2).toInt(); // 0 sell, 1 buy
                    data["rate"]            = msg.at(3).toString().toDouble();
                    data["amount"]          = msg.at(4).toString().toDouble();
                    data["total"]           = msg.at(5).toDouble();
                    tradeOrders_.append(data);
                }
                emit tradeOrdersUpdated();
            }
        }
        else
        {
            qDebug() << "Polo::handleWSSOrderBook msg type (" << msgType << ") unhandled: " <<  msg;
            return;
        }
    } // for m
}

void Polo::handleWSSFail(QString errorMessage)
{
    lastError_ = QString("WSS: %1").arg(errorMessage);
    emit failed();
}

void Polo::wssSubscribeTicker()
{
#ifndef NO_POLO_WSS
    Q_ASSERT(wss_);
    disconnect(wss_, SIGNAL(connected()), this, SLOT(wssSubscribeTicker()));
    if (wss_->isOpen())
        wss_->subscribeTicker();
#endif
}

void Polo::wssSubscribeOrderBook()
{
#ifndef NO_POLO_WSS
    Q_ASSERT(wss_);
    disconnect(wss_, SIGNAL(connected()), this, SLOT(wssSubscribeOrderBook()));
    if (wss_->isOpen())
        wss_->subscribeOrderBook(wssSubscribeCurrency_);
#endif
}


QString Polo::currencyPairByID(unsigned int pairID, const QString def /*= QString()*/)
{
    QMutexLocker locker(&mxCurrencyPairs_);
    return currencyPairs_.value(pairID, def);
}
