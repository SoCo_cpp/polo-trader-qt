#include "polowss.h"
#include <QtWebSockets>
#include <QMutexLocker>
//#include <QDebug>

PoloWSS::PoloWSS(QObject *parent) : QObject(parent)
{
    isOpen_ = false;
    webSocket_ = new QWebSocket();
    Q_ASSERT(webSocket_);
    connect(webSocket_, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(wsError(QAbstractSocket::SocketError)));
    connect(webSocket_, SIGNAL(textMessageReceived(QString)), this, SLOT(wsTextMessageReceived(QString)));
    connect(webSocket_, SIGNAL(connected()), this, SIGNAL(connected()));
    connect(webSocket_, SIGNAL(disconnected()), this, SIGNAL(disconnected()));
}

PoloWSS::~PoloWSS()
{
    if (webSocket_)
        {delete webSocket_;webSocket_ = 0;}
}

const QString& PoloWSS::lastError() const
{
    return lastError_;
}
void PoloWSS::setUrl(const QUrl& url)
{
    url_ = url;
}

bool PoloWSS::open(const QUrl& url /*= QUrl()*/)
{
    Q_ASSERT(webSocket_);
    if (url.isValid())
        url_ = url;
    else if (!url_.isValid())
    {
        lastError_ = "open validate url failed";
        return false;
    }
    webSocket_->open(url_);
    isOpen_ = true;
    return true;
}

void PoloWSS::close()
{
    Q_ASSERT(webSocket_);
    webSocket_->close();
    isOpen_ = false;
}

bool PoloWSS::isOpen()
{
    return isOpen_;
}

void PoloWSS::wsError(QAbstractSocket::SocketError error)
{
    emit failed(QString("Socket error: %1").arg((int)error));
}

void PoloWSS::wsTextMessageReceived(QString message)
{
    //qDebug() << "PoloWSS Received: " << message;
    QJsonParseError parseError;
    QJsonDocument json = QJsonDocument::fromJson(message.toLocal8Bit(), &parseError);
    if (parseError.error != QJsonParseError::NoError)
    {
        emit failed(QString("Replied data has a parsing error: %1").arg(parseError.errorString()));
        return;
    }
    if (!json.isArray())
    {
        emit failed("Replied data format is not an array");
        return;
    }

    if (json.array().size() == 2)
    {
        // subscribe/unsubscribe confirmation
        emit success(json.array());
        return;
    }

    if (!json.array().at(0).isDouble()/*isNumeric*/)
    {
        emit failed("Replied data format invalid");
        return;
    }
    int channel = (int)json.array().at(0).toDouble();

    switch (channel)
    {
        case chHeartbeat:
            emit heartbeat(json.array());
            break;
        case chTicker:
            emit ticker(json.array());
            break;
        case chVolume:
            emit volume(json.array());
            break;
        case chTrollbox:
            emit trollbox(json.array());
            break;
        default:
            // unknown channel, asumed currency ID of order book reply
            emit orderbook(json.array());
            break;
    } // switch (channel)
}

void PoloWSS::subscribeTicker()
{
    subscribe(chTicker);
}

void PoloWSS::unsubscribeTicker()
{
    unsubscribe(chTicker);
}

void PoloWSS::subscribeOrderBook(const QString& currencyPair)
{
    QJsonDocument json;
    QJsonObject jo;
    jo["command"] = "subscribe";
    jo["channel"] = currencyPair;
    json.setObject(jo);
    QString data = QString(json.toJson(QJsonDocument::Compact));
    webSocket_->sendTextMessage(data);
}

void PoloWSS::unsubscribeOrderBook(const QString& currencyPair)
{
    QJsonDocument json;
    QJsonObject jo;
    jo["command"] = "unsubscribe";
    jo["channel"] = currencyPair;
    json.setObject(jo);
    QString data = QString(json.toJson(QJsonDocument::Compact));
    webSocket_->sendTextMessage(data);
}

void PoloWSS::subscribe(int channel)
{
    QJsonDocument json;
    QJsonObject jo;
    jo["command"] = "subscribe";
    jo["channel"] = channel;
    json.setObject(jo);
    QString data = QString(json.toJson(QJsonDocument::Compact));
    webSocket_->sendTextMessage(data);
}

void PoloWSS::unsubscribe(int channel)
{
    QJsonDocument json;
    QJsonObject jo;
    jo["command"] = "unsubscribe";
    jo["channel"] = channel;
    json.setObject(jo);
    QString data = QString(json.toJson(QJsonDocument::Compact));
    webSocket_->sendTextMessage(data);
}

