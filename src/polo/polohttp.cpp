#include "polohttp.h"

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QMessageAuthenticationCode> // for crypto
#include <QUrlQuery>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
//#include <QDebug>

PoloHTTP::PoloHTTP(QObject* parent /*= 0*/, QNetworkAccessManager* networkManager /*= 0*/) : QObject(parent)
{
    netReply_ = 0;
    ownNetMan_ = (networkManager == 0);
    if (ownNetMan_)
        netMan_ = new QNetworkAccessManager();
    else
        netMan_ = networkManager;
    Q_ASSERT(netMan_);
    cryptoMac_ = new QMessageAuthenticationCode(QCryptographicHash::Sha512);
    Q_ASSERT(cryptoMac_);
    jsonReply_ = new QJsonDocument();
    Q_ASSERT(jsonReply_);
    connect(netMan_, SIGNAL(finished(QNetworkReply*)), this, SLOT(netRequestFinished(QNetworkReply*)));
}

PoloHTTP::~PoloHTTP()
{
    if (ownNetMan_ && netMan_)
        {delete netMan_;netMan_ = 0;}
    if (cryptoMac_)
        {delete cryptoMac_;cryptoMac_ = 0;}
    if (jsonReply_)
        {delete jsonReply_;jsonReply_ = 0;}
}

const QString& PoloHTTP::lastError() const
{
    return lastError_;
}

void PoloHTTP::setKeySecret(const QString& key, const QString& secret)
{
    key_ = key;
    secret_ = secret;
}

void PoloHTTP::setUrls(const QUrl& urlTrade, const QUrl& urlPublic)
{
    urlTrade_ = urlTrade;
    urlPublic_ = urlPublic;
}

bool PoloHTTP::requestPublic(const QMap<QString,QString>& data)
{
    if (!urlPublic_.isValid())
    {
        lastError_ = "request public url not valid";
        return false;
    }

    QUrl url = urlPublic_;
    // convert data map to url parameters
    QUrlQuery params;
    foreach (const QString& key, data.keys())
        params.addQueryItem(key, data.value(key, ""));
    url.setQuery(params);
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::UserAgentHeader, "Mozilla/4.0 (compatible; Poloniex Qt C++ bot;)");
    netMan_->get(request);
    return true;
}

bool PoloHTTP::requestTrade(const QMap<QString,QString>& data)
{
    Q_ASSERT(netMan_);
    Q_ASSERT(cryptoMac_);
    if (key_.isEmpty() || secret_.isEmpty())
    {
        lastError_ = "request key/secret not set";
        return false;
    }
    if (!urlTrade_.isValid())
    {
        lastError_ = "request trade url not valid";
        return false;
    }
    // convert data map to url parameters
    QUrlQuery params;
    foreach (const QString& key, data.keys())
        params.addQueryItem(key, data.value(key, ""));
    params.addQueryItem("nonce", QString::number(QDateTime::currentMSecsSinceEpoch()));
    QString postData = params.query();

    // Create MAC-SHA512 signature of post data using secret
    cryptoMac_->setKey(secret_.toLocal8Bit());
    cryptoMac_->addData(postData.toLocal8Bit());

    QNetworkRequest request(urlTrade_);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    request.setHeader(QNetworkRequest::UserAgentHeader, "Mozilla/4.0 (compatible; Poloniex Qt C++ bot;)");
    request.setRawHeader(QByteArray("Key"), key_.toLocal8Bit());
    request.setRawHeader(QByteArray("Sign"), cryptoMac_->result().toHex());

    netMan_->post(request, postData.toLocal8Bit());
    return true;
}

void PoloHTTP::netRequestFinished(QNetworkReply* reply)
{
    Q_ASSERT(jsonReply_);
    QJsonParseError parseError;
    QByteArray baReply = reply->readAll();
    qDebug() << "PoloHTTP Received: " << baReply;
    (*jsonReply_) = QJsonDocument::fromJson(baReply, &parseError);
    if (reply->error() != QNetworkReply::NoError)
    {
        lastError_ =  QString("Request replied with error: %1").arg((int)reply->error());
        if (jsonReply_->isObject() && jsonReply_->object().contains("error"))
            lastError_ += QString(" (%1)").arg(jsonReply_->object().value("error").toString());
        emit failed();
    }
    else if (parseError.error != QJsonParseError::NoError)
    {
        lastError_ = QString("Request replied data parse error: %1").arg(parseError.errorString());
        emit failed();
    }
    else emit finished();
    reply->deleteLater();
}

QJsonDocument* PoloHTTP::jsonReply()
{
    Q_ASSERT(jsonReply_);
    return jsonReply_;
}
