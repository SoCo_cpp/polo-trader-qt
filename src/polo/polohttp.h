#ifndef POLOHTTP_H
#define POLOHTTP_H

#include <QObject>
#include <QByteArray>
#include <QUrl>

class QNetworkAccessManager;
class QNetworkReply;
class QNetworkReply;
class QMessageAuthenticationCode; // for crypto

class PoloHTTP : public QObject
{
    Q_OBJECT
public:
    // supply your own network manager for parallel use or don't for automatic
    explicit PoloHTTP(QObject* parent = 0, QNetworkAccessManager* networkManager = 0);
    ~PoloHTTP();

    const QString& lastError() const;

    void setKeySecret(const QString& key, const QString& secret);
    void setUrls(const QUrl& urlTrade, const QUrl& urlPublic);

    bool requestPublic(const QMap<QString,QString>& data);
    bool requestTrade(const QMap<QString,QString>& data);

    QJsonDocument* jsonReply();

protected:
    QString lastError_;
    bool ownNetMan_;
    QNetworkAccessManager* netMan_;
    QNetworkReply* netReply_;
    QMessageAuthenticationCode* cryptoMac_;
    QString key_;
    QString secret_;
    QUrl urlTrade_;
    QUrl urlPublic_;
    QJsonDocument* jsonReply_;


signals:
    void failed();
    void finished();
public slots:
    void netRequestFinished(QNetworkReply* reply);
};

#endif // POLOHTTP_H
