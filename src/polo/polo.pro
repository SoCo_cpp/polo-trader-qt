#-------------------------------------------------
#
# Project created by QtCreator 2017-11-07T22:52:30
#
#-------------------------------------------------

#CONFIG += NoPoloWSS # unremark to dissable WSS support from Polo

QT       *= core network

TEMPLATE = lib

SOURCES  += \
            polohttp.cpp \
            polo.cpp

HEADERS  += \
            polohttp.h \
            polo.h

NoPoloWSS {
    DEFINES += NO_POLO_WSS
} else {
    QT += websockets
    SOURCES += polowss.cpp
    HEADERS += polowss.h
}

