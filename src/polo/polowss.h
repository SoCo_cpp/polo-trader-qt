#ifndef POLOWSS_H
#define POLOWSS_H

#include <QObject>
#include <QUrl>
#include <QAbstractSocket>
#include <QJsonArray>
#include <QMutex>

class QWebSocket;
class PoloWSS : public QObject
{
    Q_OBJECT
public:
    typedef enum
    {
        chTrollbox    = 1001,
        chTicker      = 1002,
        chVolume      = 1003,
        chHeartbeat   = 1010
    } Channels;

    explicit PoloWSS(QObject *parent = 0);
    ~PoloWSS();

    const QString& lastError() const;
    void setUrl(const QUrl& url);

    bool open(const QUrl& url = QUrl());
    void close();
    bool isOpen();

    // base subscribe/unsubscribe
    void subscribe(int channel);
    void unsubscribe(int channel);

protected:
    QString lastError_;
    bool isOpen_;
    QUrl url_;
    QWebSocket* webSocket_;
    QJsonArray reply_;
    QMutex mxReply_;


signals:
    void connected();
    void disconnected();
    void failed(QString errorMessage);
    void success(QJsonArray message);
    void orderbook(QJsonArray message);
    void trollbox(QJsonArray message);
    void ticker(QJsonArray message);
    void volume(QJsonArray message);
    void heartbeat(QJsonArray message);

public slots:
    void wsError(QAbstractSocket::SocketError error);
    void wsTextMessageReceived(QString message);

    // higher level subscribe/unsubscribe
    void subscribeTicker();
    void unsubscribeTicker();

    void subscribeOrderBook(const QString& currencyPair);
    void unsubscribeOrderBook(const QString& currencyPair);
};

#endif // POLOWSS_H
