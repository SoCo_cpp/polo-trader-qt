# Polo Trader Qt

Poloniex API library and usage example using Qt C++

Disclaimer: The author is not affiliated with Poloniex, Inc.

Provided by SoCo soco@socosoftware.com

Donations or tips:

 BTC: 12GGowioEsPR3tMXt6CWTgtZ6mE4e6YHbc
 
 LTC: LSnc7KRUq1b7mtF6k81eQfS9Bs4eFuZSdE
 
DOGE: D91LnrtAzaCxVCc2rmSG5CWcr7mJsQE9nw

## Description

This library implements most common Poloniex API features. Supported are most public GET API commands, authenticated trade POST API commands not related to margin trading, and the WAMP (WebSockets) push API. The example demonstrates using the library and provides a GUI to test each API request and display the interpreted reply.


## Status:

This project has completed implementing and basic testing of all targeted features. Documentation is being prepared, while more testing is done.

## Quick Start


Building the root Qt project *polotrader.pro* will compile the dynamic linked library "polo" and compile the example executable "polotrade" (i.e. *polo.dll* and *polotrade.exe* / *polo.so* and *poltrade*). There is no installer, so running the *polotrade* example will require manually moving the *polo* library where *polotrade* can find it.

Some actions require a specified currency or currency pair. To fill the drop lists with available currencies and currency pairs, first request the tickers by clicking the "Poll Tickers" button.

The two bold buttons are subscribe/unsubscribe controls for the WSS push API and will continuously update tickers and orderbooks while enabled.

To use authenticated trading API, you must specify your Poloniex API key and secret. You can simply enter the key and secret in to the text UI boxes or execute *polotrade* with your key and secret provide as command line arguments. Execute *polotrade --help* for argument details. 



### class PoloHTTP  (polo/polohttp.cpp / polo/polohttp.h)

Provides *reqestPublic* and *requestTrade* to make the two kinds of Poloneix HTTP API request. Both accept a QMap of string containing the request parameters. They both also require the API URLs to be set with *setUrls* The requestTrade requests are authenticated and additionally require your Poloniex account's API *key* and *secret* tokens to be set with *setKeySecret* before using. Either the *failed* or *finished* signal is emitted when completed. If successful, the reply data is held in a QJsonDocument object accessible with *jsonReply*.

### class PoloWSS  (polo/polowss.cpp / polo/polowss.h)

Provides subscribe and unsubscribing to tickers and specific orderbooks to a continually updating stream of pushed data. This is done with a QWebSocket and replies with signals passing the JSON reply as a parameter.


### class Polo (polo/polo.cpp / polo/polo.h)

Provides API command specific request functions and parses the JSON reply into Qt containers such as lists or maps. Polo keeps one PoloWSS object, but instantiates another PoloHTTP object for each request that is deleted after its reply is processed.


